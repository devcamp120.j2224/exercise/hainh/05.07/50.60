package com.devcamp.j01_javabasic.s50;

public class Voucher {
    public String voucherCode = "V000000";

    public String getVoucherCode(){
        return voucherCode;
    }
    public void setVoucherCode(String voucherCode){
        this.voucherCode = voucherCode;
    }
    public void showVoucher(){
        System.out.println("Voucher code is : "  + this.voucherCode);
    }
    public void showVoucher(String voucherCode){
        System.out.println("this is voucher code : " + this.voucherCode);
        System.out.println("this is voucher code too : " + voucherCode);
    }

    public static void main(String[] args) {
        Voucher voucher = new Voucher();
        //0 chạy class này rồi commend dòng code dưới 
        //voucher.showVoucher();

        // 1 . bỏ commend dòng code dưới rồi chạy lại classs này .
        //voucher.showVoucher("CODE8888");

        /* 2. commend lại dòng code trên 
         * bỏ commend 2 dòng code dưới rồi chạy lại class này 
         */
        voucher.voucherCode = "CODE2222";
        voucher.showVoucher("0008888");

        /*3. Commend lại 2 dòng code trên
        Bỏ comment các dòng code dưới rồi chạy lại class này
         */
        String code = "VOUCHER";
        voucher.setVoucherCode(code);
        System.out.println(voucher.getVoucherCode());
        voucher.showVoucher(voucher.getVoucherCode());
        voucher.showVoucher();
    }
}
